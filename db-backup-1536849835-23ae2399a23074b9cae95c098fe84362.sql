DROP TABLE tb_keb;

CREATE TABLE `tb_keb` (
  `id_keb` int(10) NOT NULL AUTO_INCREMENT,
  `id_warga` int(10) NOT NULL,
  `jenis` varchar(30) CHARACTER SET latin1 NOT NULL,
  `status` varchar(30) CHARACTER SET latin1 NOT NULL,
  `penanggung` varchar(50) CHARACTER SET latin1 NOT NULL,
  `waktu` date NOT NULL,
  PRIMARY KEY (`id_keb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE tb_login;

CREATE TABLE `tb_login` (
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO tb_login VALUES("admin","admin");



DROP TABLE tb_warga;

CREATE TABLE `tb_warga` (
  `id_warga` int(10) NOT NULL AUTO_INCREMENT,
  `no_kk` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `umur` int(2) NOT NULL,
  `jeniskelamin` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `disabilitas` varchar(30) NOT NULL,
  `alatbantu` varchar(30) NOT NULL,
  `dusun` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `longtitude` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_warga`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO tb_warga VALUES("1","3401234","Yosafat","22","L","-","-","Maguwoharjo","Kembang Maguwoharjo","foto/g6.jpg","-7.815532","110.162667");
INSERT INTO tb_warga VALUES("2","340489765","Sandi","21","L","a","b","Kembang","Kembang","foto/g6.jpg","-7.815531","112.162667");



