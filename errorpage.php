<!DOCTYPE HTML>
<html>
<head>
<title>PAGE NOT FOUND</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/morris.css" type="text/css"/>
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="css/jquery-ui.css"> 
<!-- jQuery -->
<script src="js/jquery-2.1.4.min.js"></script>
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
</head> 
<body>
	<div class="main">
	<div class="container">
		<div class="agile">
			
			<div class="w3l-txt">
				<div class="text">
					<h1>PAGE NOT FOUND</h1>	
					
					<p>You have been tricked into click on a link that can not be found. Please check the url or go to <a href="./">main page</a> and see if you can locate what you are looking for</p>
				</div>
				<div class="image">
					<img src="images/smile.png">
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="back">
						<a href="./">Back to home</a>
				</div>
				<div class="footer">
					<p>&copy; 2017 Yakkum</p>
				</div>
				
					
	</div>
	</div>
	</div>
</body>
</html>